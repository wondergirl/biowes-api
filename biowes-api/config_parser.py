from ConfigParser import ConfigParser
import unittest

__author__ = 'Dmytro Soloviov {dsoloviov@frov.jcu.cz}'

config = ConfigParser()
config.readfp(open('config.ini'))

srv = config.get('DATABASE', 'Server')
usr = config.get('DATABASE', 'Username')
psw = config.get('DATABASE', 'Password')
dbs = config.get('DATABASE', 'Database')
hst = config.get('CONNECTION', 'Host')


class TestConfig(unittest.TestCase):

    def test_srv(self):
        self.assertEqual(srv, '160.217.215.198')

    def test_usr(self):
        self.assertEqual(usr, 'sa')

    def test_psw(self):
        self.assertEqual(psw, 'Sa2014')

    def test_dbs(self):
        self.assertEqual(dbs, 'DP_BIOWES')

    def test_hst(self):
        self.assertEqual(hst, '0.0.0.0')

if __name__ == '__main__':
    unittest.main()