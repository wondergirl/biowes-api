from flask import jsonify, request
from models.User import User

import logging
__author__ = 'Ann'

def users():
    user = User()
    return str(user.get_users())

def uid():
    user = User()
    return str(user.get_uid())

def user(uid):
    user = User()
    i = user.get_user(uid)
    return jsonify({'id': i[0][0], 'email': i[0][1], 'fname': i[0][2], 'lname': i[0][3]})