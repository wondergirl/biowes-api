import pymssql as sql
import unittest
import config_parser as config

__author__ = 'Dmytro Soloviov {dsoloviov@frov.jcu.cz}'
__author__ = 'ann'


class DatabaseManager(object):
    """
    Microsoft SQL database interface
    """

    # one instance of database connection
    def __new__(cls, *args, **kwds):
        conn = cls.__dict__.get("__conn__")
        if conn is not None:
            print("already")
            return conn
        cls.__conn__ = conn = object.__new__(cls)
        conn.init(*args, **kwds)
        return conn

    def init(self, *args, **kwds):
        self.conn = sql.connect(config.srv, config.usr, config.psw, config.dbs).cursor()
        print("connected")
        pass

    def cursor(self):
        return self.connection.cursor()

    def get_connection(self):
        return self.cursor()

    def connect(self, server, user, password, database):
        self.server = server
        self.user = user
        self.password = password
        self.database = database
        return self


class TestConnection(unittest.TestCase):
    def setUp(self):
        import config_parser as c
        self.db = DatabaseManager(c.srv, c.usr, c.psw, c.dbs)
        self.cursor = self.db.cursor()

    def test_version(self):
        self.cursor.execute('SELECT @@VERSION')
        self.assertEqual(self.cursor.fetchone()[0][:25], 'Microsoft SQL Server 2014')


if __name__ == '__main__':
    unittest.main()
