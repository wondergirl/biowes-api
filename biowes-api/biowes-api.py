from flask import Flask
from flask import render_template
from flask import request, abort
import controllers.UserController as UserController
import re
import logging
# import config_parser as c

__author__ = 'Dmytro Soloviov {dsoloviov@frov.jcu.cz}'
__author__ = 'ann'


app = Flask(__name__)
logFile = u'log.log'

# Routes
# The parameters name should be the same as the ones used in the controller method
routes = {
    'api/users$': UserController.users,
    'api/uid': UserController.uid,
    'api/users/(?P<uid>\d+)': UserController.user
}

@app.route('/', defaults={'path': ''})
@app.route('/<path:path>')
def main(path):
    # Logging to logFile
    format = '%(levelname)-8s Client: ' + request.remote_addr + '   %(message)s'
    logging.basicConfig(format = format,
                         level = logging.INFO,
                         filename = logFile)
    logging.warning("")

    # Checking routes
    for rexp, controller in routes.iteritems():
            m = re.match(rexp, path)
            if m is not None:
                params = m.groupdict()
                return controller(**params)
    abort(400)

def placeholder():
    return render_template('index.html')


if __name__ == '__main__':

    app.debug = True
    app.run()
