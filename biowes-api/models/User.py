__author__ = 'Ann'

from database_manager import DatabaseManager


class User(DatabaseManager):

    def get_user(self, uid):
        self.conn.execute("""
        SELECT ALL [ID_USER]
        ,[U_EMAIL]
        ,[U_FIRST_NAME]
        ,[U_LAST_NAME]
        FROM [DP_BIOWES].[dbo].[SYS_USERS]
        WHERE [ID_USER] = %s
        """ % uid)
        return self.conn.fetchall()

    def get_users(self):
        self.conn.execute("""
        SELECT ALL [ID_USER]
        ,[U_EMAIL]
        ,[U_FIRST_NAME]
        ,[U_LAST_NAME]
        FROM [DP_BIOWES].[dbo].[SYS_USERS]
        """)
        return self.conn.fetchall()

    def get_uid(self):
        self.conn.execute("""
        SELECT ALL [ID_USER]
        FROM [DP_BIOWES].[dbo].[SYS_USERS]
        """)
        return self.conn.fetchall()

