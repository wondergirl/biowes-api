import requests

for uid in (115, 116):
    r = requests.get('http://160.217.215.198:5000/api/users/%s' % uid)
    content = r.json()

    print('USER #%s:' % uid)
    print('\tID: %s' % content['id'])
    print('\tEmail: %s' % content['email'])
    print('\tName: %s %s' % (content['fname'], content['lname']))
    print('\n')

raw_input('\n\nPress ENTER to quit...')
