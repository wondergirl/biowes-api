#/usr/bin/env python

import pymssql as sql

srv = '160.217.215.198'
usr = 'sa'
psw = 'Sa2014'


def main(command):
    # Routing
    func = {'users': get_users, 'protocols': get_protocols, 'templates': get_templates}
    x = func[command]()
    return x


def connect():
    conn = sql.connect(srv, usr, psw, 'DP_BIOWES')
    return conn.cursor()


def get_user(uid):
    cursor = connect()
    cursor.execute("""
    SELECT ALL [U_EMAIL]
        ,[U_FIRST_NAME]
        ,[U_LAST_NAME]
    FROM [DP_BIOWES].[dbo].[SYS_USERS]
    WHERE [ID_USER] = %s
    """ % uid)
    user = cursor.fetchall()
    print(user)


def get_users():
    cursor = connect()
    cursor.execute("""
    SELECT TOP 1000 [ID_USER]
        ,[U_EMAIL]
        ,[U_FIRST_NAME]
        ,[U_LAST_NAME]
    FROM [DP_BIOWES].[dbo].[SYS_USERS]
    """)

    users = cursor.fetchall()
    return users


def get_protocols():
    cursor = connect()
    cursor.execute("""
    SELECT TOP 1000 [ID_RTM_PROTOCOL_HEAD]
        ,[PH_CREATE_AUTHOR]
        ,[PH_NAME]
    FROM [DP_BIOWES].[dbo].[RTM_PROTOCOL_HEADS]
    """)

    protocols = cursor.fetchall()
    return protocols


def get_templates():
    cursor = connect()
    cursor.execute("""
    SELECT TOP 1000 [ID_DEF_PROTOCOL_HEAD]
        ,[PH_CREATE_AUTHOR]
        ,[PH_NAME]
    FROM [DP_BIOWES].[dbo].[DEF_PROTOCOL_HEADS]
    """)

    templates = cursor.fetchall()
    return templates


if __name__ == '__main__':
#    info = main('users')
#    for i in info:
#        print('\tID: %s\n\tEMAIL: %s\n\tNAME: %s %s\n' % (i[0], i[1], i[2], i[3]))
    get_user(116)
