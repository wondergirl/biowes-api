from flask import Flask
from flask import jsonify
from db import main as db
app = Flask(__name__)


@app.route('/api/')
def main():
    return 'BioWes API: Main page'


@app.route('/api/web_users', methods=['GET'])
def web_users():
    users = db('users')
    return str(users)


@app.route('/api/users', methods=['GET'])
def users():
    users = db('users')
    return jsonify({'id': users[0][0], 'email': users[0][1], 'fname': users[0][2], 'lname': users[0][3]})


@app.route('/api/web_protocols', methods=['GET'])
def web_protocols():
    protocols = db('protocols')
    return str(protocols)


@app.route('/api/protocols', methods=['GET'])
def protocols():
    protocols = db('protocols')
    return jsonify({'id': protocols[0][0], 'email': protocols[0][1], 'name': protocols[0][2]})


@app.route('/api/web_templates', methods=['GET'])
def web_templates():
    templates = db('templates')
    return str(templates)


@app.route('/api/templates', methods=['GET'])
def templates():
    templates = db('templates')
    return jsonify({'id': templates[0][0], 'email': templates[0][1], 'name': templates[0][2]})


if __name__ == "__main__":
#    app.debug = True
#    app.run(host='0.0.0.0')  # public
    app.run()  # private
